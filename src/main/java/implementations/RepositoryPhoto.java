/*
 Dodawanie description i Date do listy aby mo�na by�o ich szuka�
 */

package implementations;

import java.sql.Connection;
import java.sql.SQLException;



import unitOfWork.IUnitOfWork;
import domain.Photo;
import entityBuilders.IEntityBuilder;

public class RepositoryPhoto extends RepositoryBase<Photo>
{

	protected RepositoryPhoto(Connection connection, IEntityBuilder<Photo> builder, IUnitOfWork uow)
	{
		super(connection, builder, uow);
	}
	
	@Override
	protected void prepareUpdateQuery(Photo entity) throws SQLException
	{
		  	update.setString(1, (entity.getDescription()));
	        update.setInt(2, entity.getDate());

	}
	
	@Override
	protected void prepareAddQuery(Photo entity) throws SQLException 
	{
			save.setString(1, (entity.getDescription()));
		 	save.setInt(2, entity.getDate());
	}
	
	@Override
	protected String getTableName() {
		
		return "photo";
	}

	@Override
	protected String getUpdateQuery()
	{
		return "UPDATE photo SET description=?, date=? WHERE id=?";
	}

	@Override
	protected String getCreateQuery() 
	{
		
		return "INSERT INTO photo(description , date) VALUES (?, ?)";
	}
	
}
	
