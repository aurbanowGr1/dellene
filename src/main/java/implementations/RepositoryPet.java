
package implementations;

import java.sql.Connection;
import java.sql.SQLException;




import unitOfWork.IUnitOfWork;
import domain.Pet;
import entityBuilders.IEntityBuilder;


public class RepositoryPet extends RepositoryBase<Pet>
{

	public RepositoryPet(Connection connection, IEntityBuilder<Pet> builder, IUnitOfWork uow)
	{
		super(connection, builder, uow);
	}
	
	@Override
	protected void prepareUpdateQuery(Pet entity) throws SQLException
	{
		  	update.setString(1, (entity.getName()));
	        update.setInt(2, entity.getAge());
	        update.setString(3, entity.getBreed());
	        update.setString(4, entity.getSpecies());

	}
	
	@Override
	protected void prepareAddQuery(Pet entity) throws SQLException 
	{
			save.setString(1, (entity.getName()));
		 	save.setInt(2, entity.getAge());
	        save.setString(3, entity.getBreed());
	        save.setString(4, entity.getSpecies());
	}
	
	@Override
	protected String getTableName() {
		
		return "pet";
	}

	@Override
	protected String getUpdateQuery()
	{
		return "UPDATE pet SET name=?, age=?, breed=?, species=? WHERE id=?";
	}

	@Override
	protected String getCreateQuery() 
	{
		
		return "INSERT INTO pet(name, age, breed, species) VALUES (?, ?, ?,?)";
	}
}
