/*wyszukiwanie poprzez utworzenie list*/

package Repositories;
import domain.User;

public interface IRepositoryUser extends IRepository<User>{

	public User getUserByLogin(String login);
	public User getUserByEmail(String email);
	public User getUserByName(String name);
	public User getUserByCity(String City);
	public User getUserByAge(int age);
	public User getUserBySurname(String surname);
        
}
