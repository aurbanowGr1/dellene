/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Repositories;

import domain.Pet;
import domain.Photo;

public interface IRepositoryPhoto extends IRepository<Photo>{
    
	public Pet getByDescription(String description);
	public Pet getBDate(int date);
	
    
}
