package domain;

/**
        Informacje dotyczące zwierzęcia - imie, wiek, rasa.
     */

public class Pet extends Entity {

	public String name;
	public int age;
        public String breed;
	public String species;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getBreed() {
		return breed;
	}
	public void setBreed(String breed) {
		this.breed = breed;
	}
	public String getSpecies() {
		return species;
	}
	public void setSpecies(String species) {
		this.species = species;
	}
	
	
}
