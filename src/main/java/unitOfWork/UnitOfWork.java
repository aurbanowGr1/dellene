package unitOfWork;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import domain.Entity;
import domain.EntityState;

public class UnitOfWork implements IUnitOfWork
{

	private Map<Entity,IUnitOfWorkRepository> entities = new HashMap<Entity,IUnitOfWorkRepository>();
	
	Connection connection;
	
	public UnitOfWork(Connection connection)
	{
		this.connection=connection;
		try 
		{
			connection.setAutoCommit(false);
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void commit() {
		
		try {
			for(Entity entity: entities.keySet())
			{
				switch(entity.getState())
				{
				case Changed:
					entities.get(entity).persistUpdate(entity);
					break;
					
				case Deleted:
					entities.get(entity).persistDelete(entity);
					break;
				case New:
					entities.get(entity).persistAdd(entity);
					break;
				case Unchanged:
					break;
				default:
					break;
				}
			}
			connection.commit();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}

	public void rollback() 
	{
		entities.clear();
	}

	public void markAsNew(Entity entity, IUnitOfWorkRepository repository) 
	{
		entity.setState(EntityState.New);
		entities.put(entity, repository);
		
	}

	public void markAsChanged(Entity entity, IUnitOfWorkRepository repository) 
	{
		entity.setState(EntityState.Changed);
		entities.put(entity, repository);
		
	}

	public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository)
	{
		entity.setState(EntityState.Deleted);
		entities.put(entity, repository);
	}

}