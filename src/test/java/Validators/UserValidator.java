
package Validators;

import domain.*;

public class UserValidator {
    
    public static User createUser( int id,
			String login,
			String password,
                        String name,
                        String surname,
                        int age,
                        String city,
                        String email){
		
		User createUser = new User();
		
	//	createUser.setID(id);
		createUser.setLogin(login);
		createUser.setPassword(password);
                createUser.setName(name);
                createUser.setSurname(surname);
                createUser.setAge(age);
                createUser.setCity(city);
                createUser.setEmail(email);
		
		return createUser;
	}
    
    
}
