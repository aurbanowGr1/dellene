package implementationTests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.Test;

import Repositories.IRepository;
import implementations.RepositoryUser;
import unitOfWork.IUnitOfWork;
import unitOfWork.UnitOfWork;
import domain.User;
import entityBuilders.IEntityBuilder;
import entityBuilders.UserEntityBuilder;

public class RepositoryUserTest
{

    @Test
    public void testPrepareUpdateQuery() throws Exception 
    {
        try 
        {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=root");
        IEntityBuilder<User> builder = new UserEntityBuilder();
        IUnitOfWork uow = new UnitOfWork(connection);
        
        IRepository<User> repo = new RepositoryUser(connection,builder, uow);
        User u = new User();
        u.setLogin("Malinn");                     
        u.setPassword("root123");
        repo.add(u);
        assertNotNull(u);
        User u1 = new User();
        u1.setLogin("Cosik");                  
        u1.setPassword("password");
        repo.add(u1);
        u.setLogin("Dellene");
        repo.update(u);
        uow.commit();
        assertNotSame(repo.get(61), repo.get(62));
        System.out.println(u.getLogin());
        connection.close();
        } 
        catch (Exception e) 
        {
        e.printStackTrace();
        }
}

    @Test
    public void testPrepareAddQuery() throws Exception 
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/test?user=root&password=root");
            IEntityBuilder<User> builder = new UserEntityBuilder();
            IUnitOfWork uow = new UnitOfWork(connection);
            IRepository<User> repo = new RepositoryUser(connection,builder, uow);
            User p = new User();
            p.setLogin("Login");
            p.setPassword("cos");
            repo.add(p);
            uow.commit();
            connection.close();
        } 
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}